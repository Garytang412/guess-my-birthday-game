from random import randint

# assign input to name variable:
name = input('Hi! What is your name?')
# set a counter for loop:
counter = 1

# main function:
while counter <=4 :
    # assign random month, year then print them out:
    guess_month,guess_year = randint(1,12),randint(1924,2004)
    print(f"Guess {counter} : {name} were you born in {guess_month}/{guess_year} ?")

    # request player's input:
    answer = input('yes or no?')

    # check if input is valid:
    while answer not in ['yes','no']:
        answer = input("invalid input, please enter 'yes' or 'no'")

    # condition reaction:
    if answer == 'yes':
        print('I knew it!')
        break
    else:
        print('Drat! Lemme try again!')
    
    # counter +1 each time computer tried:
    counter += 1
# counter == 5 means computer tried 4 time, and not get the right birthday:
if counter == 5:
        print('I have other things to do. Good bye.')